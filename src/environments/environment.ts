// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyAiBt70GSCG5UBLZpCl8jQmg3q7U65Amvg",
    authDomain: "hello-2d21d.firebaseapp.com",
    databaseURL: "https://hello-2d21d.firebaseio.com",
    projectId: "hello-2d21d",
    storageBucket: "hello-2d21d.appspot.com",
    messagingSenderId: "983566953259",
    appId: "1:983566953259:web:7d3affc4178901d90a736e"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
