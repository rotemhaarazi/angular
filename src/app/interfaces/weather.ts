import { StringifyOptions } from 'querystring';

export interface Weather {
    [x: string]: any;
    name:string,
    country:string,
    image:string,
    description:string,
    temperature:number,
    lat?:number,
    lon?:number
}
